# README

EP3-OO(UnB-Gama)

O objetivo do projeto foi desenvolver um catalogo web de veículos , ultilizando o ruby on rails.


INTRODUÇÃO:


    O usuário poderá navegar pelo site, onde possuirá a opção de visualizar produtos e seus detalhes e descrever um veículo.
    
    A aplicação possui 2 niveis de acesso. O primeiro é o nivel de acesso do usuarios não cadastrado, onde poderá  visualizar os produtos e suas descrições e descrever veiculo.
    O segundo nível é o do administrador, que terá a capacidade de cadastrar, editar e excluir os produtos, visualizar veiculos descritos pelos usuarios, visualizar e excluir descrições de veiculos dos usuaros.


DETALHES:


    ruby 2.5.3
    Rails 5.2.1.1
    
    
DADOS DO ALUNO:

Nome: Moacir Mascarenha Soares Junior

Matricula: 17/0080366

git: https://gitlab.com/Moacir_msj/ep3