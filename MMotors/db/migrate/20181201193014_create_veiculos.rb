class CreateVeiculos < ActiveRecord::Migration[5.2]
  def change
    create_table :veiculos do |t|
      t.string :modelo
      t.string :marca
      t.string :cor
      t.float :preco
      t.string :combustivel
      t.string :transmissao
      t.string :potencia
      t.text :informacao_adicional

      t.timestamps
    end
  end
end
