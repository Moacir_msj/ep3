class CreateDveiculos < ActiveRecord::Migration[5.2]
  def change
    create_table :dveiculos do |t|
      t.string :modelo
      t.string :marca
      t.string :cor
      t.string :freio
      t.string :painel
      t.text :info_adicional

      t.timestamps
    end
  end
end
