require 'test_helper'

class DveiculosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dveiculo = dveiculos(:one)
  end

  test "should get index" do
    get dveiculos_url
    assert_response :success
  end

  test "should get new" do
    get new_dveiculo_url
    assert_response :success
  end

  test "should create dveiculo" do
    assert_difference('Dveiculo.count') do
      post dveiculos_url, params: { dveiculo: { cor: @dveiculo.cor, freio: @dveiculo.freio, info_adicional: @dveiculo.info_adicional, marca: @dveiculo.marca, modelo: @dveiculo.modelo, painel: @dveiculo.painel } }
    end

    assert_redirected_to dveiculo_url(Dveiculo.last)
  end

  test "should show dveiculo" do
    get dveiculo_url(@dveiculo)
    assert_response :success
  end

  test "should get edit" do
    get edit_dveiculo_url(@dveiculo)
    assert_response :success
  end

  test "should update dveiculo" do
    patch dveiculo_url(@dveiculo), params: { dveiculo: { cor: @dveiculo.cor, freio: @dveiculo.freio, info_adicional: @dveiculo.info_adicional, marca: @dveiculo.marca, modelo: @dveiculo.modelo, painel: @dveiculo.painel } }
    assert_redirected_to dveiculo_url(@dveiculo)
  end

  test "should destroy dveiculo" do
    assert_difference('Dveiculo.count', -1) do
      delete dveiculo_url(@dveiculo)
    end

    assert_redirected_to dveiculos_url
  end
end
