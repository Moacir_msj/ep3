require 'test_helper'

class EveiculosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @eveiculo = eveiculos(:one)
  end

  test "should get index" do
    get eveiculos_url
    assert_response :success
  end

  test "should get new" do
    get new_eveiculo_url
    assert_response :success
  end

  test "should create eveiculo" do
    assert_difference('Eveiculo.count') do
      post eveiculos_url, params: { eveiculo: { cor: @eveiculo.cor, freio: @eveiculo.freio, info: @eveiculo.info, marca: @eveiculo.marca, nome: @eveiculo.nome, painel: @eveiculo.painel } }
    end

    assert_redirected_to eveiculo_url(Eveiculo.last)
  end

  test "should show eveiculo" do
    get eveiculo_url(@eveiculo)
    assert_response :success
  end

  test "should get edit" do
    get edit_eveiculo_url(@eveiculo)
    assert_response :success
  end

  test "should update eveiculo" do
    patch eveiculo_url(@eveiculo), params: { eveiculo: { cor: @eveiculo.cor, freio: @eveiculo.freio, info: @eveiculo.info, marca: @eveiculo.marca, nome: @eveiculo.nome, painel: @eveiculo.painel } }
    assert_redirected_to eveiculo_url(@eveiculo)
  end

  test "should destroy eveiculo" do
    assert_difference('Eveiculo.count', -1) do
      delete eveiculo_url(@eveiculo)
    end

    assert_redirected_to eveiculos_url
  end
end
