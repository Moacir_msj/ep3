require "application_system_test_case"

class DveiculosTest < ApplicationSystemTestCase
  setup do
    @dveiculo = dveiculos(:one)
  end

  test "visiting the index" do
    visit dveiculos_url
    assert_selector "h1", text: "Dveiculos"
  end

  test "creating a Dveiculo" do
    visit dveiculos_url
    click_on "New Dveiculo"

    fill_in "Cor", with: @dveiculo.cor
    fill_in "Freio", with: @dveiculo.freio
    fill_in "Info Adicional", with: @dveiculo.info_adicional
    fill_in "Marca", with: @dveiculo.marca
    fill_in "Modelo", with: @dveiculo.modelo
    fill_in "Painel", with: @dveiculo.painel
    click_on "Pronto"

    assert_text "Dveiculo was successfully created"
    click_on "Back"
  end

  test "updating a Dveiculo" do
    visit dveiculos_url
    click_on "Edit", match: :first

    fill_in "Cor", with: @dveiculo.cor
    fill_in "Freio", with: @dveiculo.freio
    fill_in "Info Adicional", with: @dveiculo.info_adicional
    fill_in "Marca", with: @dveiculo.marca
    fill_in "Modelo", with: @dveiculo.modelo
    fill_in "Painel", with: @dveiculo.painel
    click_on "Update Dveiculo"

    assert_text "Dveiculo was successfully updated"
    click_on "Back"
  end

  test "destroying a Dveiculo" do
    visit dveiculos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Dveiculo was successfully destroyed"
  end
end
