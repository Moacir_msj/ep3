json.extract! veiculo, :id, :modelo, :marca, :cor, :preco, :combustivel, :transmissao, :potencia, :informacao_adicional, :created_at, :updated_at
json.url veiculo_url(veiculo, format: :json)
