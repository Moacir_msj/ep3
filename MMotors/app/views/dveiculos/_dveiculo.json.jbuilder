json.extract! dveiculo, :id, :modelo, :marca, :cor, :freio, :painel, :info_adicional, :created_at, :updated_at
json.url dveiculo_url(dveiculo, format: :json)
