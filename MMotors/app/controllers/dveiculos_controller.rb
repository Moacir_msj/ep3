class DveiculosController < ApplicationController
  before_action :set_dveiculo, only: [:show, :edit, :update, :destroy]


  # GET /dveiculos
  # GET /dveiculos.json
  def index
    @dveiculos = Dveiculo.all
  end

  # GET /dveiculos/1
  # GET /dveiculos/1.json
  def show
  end

  # GET /dveiculos/new
  def new
    @dveiculo = Dveiculo.new
  end

  # GET /dveiculos/1/edit
  def edit
  end

  # POST /dveiculos
  # POST /dveiculos.json
  def create
    @dveiculo = Dveiculo.new(dveiculo_params)

    respond_to do |format|
      if @dveiculo.save
        format.html { redirect_to @dveiculo, notice: 'Dveiculo was successfully created.' }
        format.json { render :show, status: :created, location: @dveiculo }
      else
        format.html { render :new }
        format.json { render json: @dveiculo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dveiculos/1
  # PATCH/PUT /dveiculos/1.json
  def update
    respond_to do |format|
      if @dveiculo.update(dveiculo_params)
        format.html { redirect_to @dveiculo, notice: 'Dveiculo was successfully updated.' }
        format.json { render :show, status: :ok, location: @dveiculo }
      else
        format.html { render :edit }
        format.json { render json: @dveiculo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dveiculos/1
  # DELETE /dveiculos/1.json
  def destroy
    @dveiculo.destroy
    respond_to do |format|
      format.html { redirect_to dveiculos_url, notice: 'Dveiculo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dveiculo
      @dveiculo = Dveiculo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dveiculo_params
      params.require(:dveiculo).permit(:modelo, :marca, :cor, :freio, :painel, :info_adicional, :nome, :telefone ,:email )
    end
end
