class Dveiculo < ApplicationRecord
	validates_presence_of :nome
    validates_presence_of :email
    validates_presence_of :telefone
    validates_presence_of :modelo
    validates_presence_of :info_adicional
    
end
