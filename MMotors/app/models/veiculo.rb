class Veiculo < ApplicationRecord
	validates_presence_of :modelo
    validates_presence_of :marca
    validates_presence_of :preco
    validates_presence_of :cor
    validates_presence_of :combustivel
    validates_presence_of :transmissao
    validates_presence_of :potencia
    validates_presence_of :imagem

end
