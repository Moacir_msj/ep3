Rails.application.routes.draw do
  resources :dveiculos
  resources :veiculos
  devise_for :users
  get "veiculos/contato"
	root to: "veiculos#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
